using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;
using UnityEngine.UI;


public class Laucher : MonoBehaviourPunCallbacks
{
    #region variables
    public static Laucher Instance;

    [SerializeField] private GameObject[] screenObject;
    [SerializeField] private TMP_Text infoTexto;
    [SerializeField] private TMP_Text nameRoom;
    [SerializeField] private TMP_InputField roomNameInpit;
    [SerializeField] private TMP_Text error;

    [SerializeField] private Image img_error;
    [SerializeField] private float multiplier;

    [SerializeField] private GameObject prefboton;
    [SerializeField] private Transform content;
    [SerializeField] private List<RoomButton> roombutons = new List<RoomButton>();
    private IEnumerator fade;

    public float tiempo;

    #endregion

    #region funciones 

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    void Start()
    {

        infoTexto.text = "Connectig to network....";
        SetStarScreen(0);
        PhotonNetwork.ConnectUsingSettings();
    }

    public void SetStarScreen(int indeX)
    {
        for (int i = 0; i < screenObject.Length; i++)
        {
            screenObject[i].SetActive(i == indeX);
            PhotonNetwork.ConnectUsingSettings();
        }
    }
    void Update()
    {

    }
    public void CreateRoom()
    {
        if (!string.IsNullOrEmpty(roomNameInpit.text))
        {
            RoomOptions options = new RoomOptions();
            options.MaxPlayers = 8;
            PhotonNetwork.CreateRoom(roomNameInpit.text);
            infoTexto.text = "Creating Room...";
            SetStarScreen(0);
        }
    }
    #endregion

    #region Photon

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        
        for (int i = 0; i < roomList.Count; i++)
        {
            if(roomList[i].PlayerCount!=roomList[i].MaxPlayers&& !roomList[i].RemovedFromList)
            {
                RoomButton newroombutton = Instantiate(prefboton, content).GetComponent<RoomButton>();
                newroombutton.SetButtonDetaild(roomList[i]);
                roombutons.Add(newroombutton);
            }

            if(roomList[i].RemovedFromList)
            {
                for (int J = 0; J < roombutons.Count; J++)
                {
                     if(roombutons[J].roomInfo.Name == roomList[i].Name)
                     {
                        GameObject go = roombutons[J].gameObject;
                        Destroy(go);
                     }
                }
            }
        }
    }
    public void JoinRoom(RoomInfo info)
    {
        PhotonNetwork.JoinRoom(info.Name);
        infoTexto.text = "Joining Room...";
        SetStarScreen(0);
    }
    public void RetunMenu()
    {
        PhotonNetwork.LeaveRoom();
        //PhotonNetwork.LeaveLobby();
        SetStarScreen(1);
    }
    
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        //PhotonNetwork.LeaveLobby();
        SetStarScreen(1);
    }
    public override void OnJoinedRoom()
    {
        nameRoom.text = PhotonNetwork.CurrentRoom.Name;
        SetStarScreen(4);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        error.text = "El nombre de la sala ya existe ";
        StartFade(0,1);
        SetStarScreen(5);
    }

    void StartFade(float startPoint, float endPoint) 
    {
        if (fade != null)
        {
            StopCoroutine(fade);
        }

        fade = Fade(startPoint, endPoint);
        StartCoroutine(fade);
    }

    private IEnumerator Fade(float startPoint, float endpoint)
    {
        Color c = img_error.color;
  
        while (startPoint !=  endpoint)
        {
            startPoint = Mathf.Clamp(startPoint, endpoint,Time.deltaTime+tiempo);
            c.a = startPoint;
            img_error.color = c;
            Debug.Log(startPoint);
            yield return null;
        }

        if (startPoint == 0)
        {
            StartFade(0, 1);
            
        }
        else
        {
           StartFade(1, 0);
        }

    }
    #endregion
}
