using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using TMPro;
public class RoomButton : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public  RoomInfo roomInfo;
    [SerializeField] private TMP_Text texxto;

    #region Custon Funtions

    public void SetButtonDetaild(RoomInfo inputInfo)
    {
        roomInfo = inputInfo;
        texxto.text = roomInfo.Name;
    }

    public void JoinButtonRoom()
    {
        Laucher.Instance.JoinRoom(roomInfo);
       
    }

    #endregion
}

